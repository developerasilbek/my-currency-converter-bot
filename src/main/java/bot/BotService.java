package bot;

import com.itextpdf.text.DocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.io.IOException;
import java.util.List;

public interface BotService {

    SendMessage generateSendMessageWithInlineKeyboardMarkup(String chatId, String text, InlineKeyboardMarkup markup);

    InlineKeyboardMarkup generateInlineKeyboardMarkup(int elementOfRow, List<String> strings, String prefix);

    SendDocument sendExcel(Update update) throws IOException, InvalidFormatException;

    SendDocument sendPdf(Update update) throws IOException, DocumentException;
}
