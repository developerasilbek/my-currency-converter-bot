package bot;

import com.itextpdf.text.DocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import service.FileService;
import service.FileServiceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BotServiceImpl implements BotService{

    public static FileService fileService = new FileServiceImpl();

    public SendMessage generateSendMessageWithInlineKeyboardMarkup(String chatId, String text, InlineKeyboardMarkup markup) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(text);
        if (markup != null) {
            sendMessage.setReplyMarkup(markup);
        }
        return sendMessage;
    }

    public InlineKeyboardMarkup generateInlineKeyboardMarkup(int elementOfRow, List<String> strings, String prefix) {
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        List <List<InlineKeyboardButton>> rowList = new ArrayList<>();
        List <InlineKeyboardButton> row1 = new ArrayList<>();

        for (int i = 0; i < strings.size(); i++) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(strings.get(i));
            button.setCallbackData(prefix+strings.get(i));
            row1.add(button);
            if ((i+1) % elementOfRow == 0){
                rowList.add(row1);
                row1=new ArrayList<>();
            }
        }
        if (strings.size() % elementOfRow != 0){
            rowList.add(row1);
        }
        markup.setKeyboard(rowList);
        return markup;
    }

    public SendDocument sendExcel(Update update) throws IOException, InvalidFormatException {
        String chatId = update.getCallbackQuery().getMessage().getChatId().toString();
        SendDocument sendDocument = new SendDocument();
        sendDocument.setChatId(chatId);
        InputFile inputFile = new InputFile();
        inputFile.setMedia(fileService.GenerateExcel());
        sendDocument.setDocument(inputFile);
        return sendDocument;
    }

    public SendDocument sendPdf(Update update) throws IOException, DocumentException {
        String chatId = update.getCallbackQuery().getMessage().getChatId().toString();
        SendDocument sendDocument = new SendDocument();
        sendDocument.setChatId(chatId);
        InputFile inputFile = new InputFile();
        inputFile.setMedia(fileService.GeneratePdf());
        sendDocument.setDocument(inputFile);
        return sendDocument;
    }


}
