package bot;

import lombok.SneakyThrows;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.util.ArrayList;
import java.util.List;

public class CurrencyConverterBot extends TelegramLongPollingBot {

    /** Sanaqulov Asilbek 4-VARIANT **/

    public static BotService botService = new BotServiceImpl();

    @Override
    public String getBotUsername() {
        return "valyutaexambot";
    }

    @Override
    public String getBotToken() {
        return "5444306308:AAG3fhQUmx39eouq9tQCjwwPxkQ6rbfC2E8";
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {

        SendMessage sendMessage = new SendMessage();

        if (update.getMessage() != null) {
            String chatId = String.valueOf(update.getMessage().getChatId());
            String text = update.getMessage().getText();
            sendMessage.setChatId(chatId);
            if (text.equals("/start")) {
                List<String> list = new ArrayList<>();
                list.add("download Excel");
                list.add("download Pdf");
                /** Ushbu method 2 ta buttondan iborat InlineKeyboard yaratib beradi **/
                InlineKeyboardMarkup inlineKeyboardMarkup = botService.generateInlineKeyboardMarkup(2, list, "kurs:");
                execute(botService.generateSendMessageWithInlineKeyboardMarkup(chatId, "File tanlang:", inlineKeyboardMarkup));
            }

        } else if (update.getCallbackQuery() != null) {
            String chatId = String.valueOf(update.getCallbackQuery().getMessage().getChatId());
            String data = update.getCallbackQuery().getData();
            sendMessage.setChatId(chatId);
            /** Excel file yuborish **/
            if (data.endsWith("Excel")) {
                SendDocument sendDocument = botService.sendExcel(update);
                sendDocument.setCaption("Currency Rate Excel");
                execute(sendDocument);

            }
            /** Pdf file yuborish **/
            else if (data.endsWith("Pdf")) {
                SendDocument sendDocument = botService.sendPdf(update);
                sendDocument.setCaption("Currency Rate Pdf");
                execute(sendDocument);
            }

            execute(sendMessage);
        }
    }

}
