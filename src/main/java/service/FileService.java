package service;

import com.itextpdf.text.DocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.File;
import java.io.IOException;

public interface FileService {

    File GenerateExcel() throws IOException, InvalidFormatException;
    File GeneratePdf() throws IOException, DocumentException;
}
