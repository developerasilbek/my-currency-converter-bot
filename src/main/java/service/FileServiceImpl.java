package service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import model.ResponseCurrency;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import utils.AppConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class FileServiceImpl implements FileService{

        public static UrlService urlService = new UrlServiceImpl();

        public File GenerateExcel() throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sh = workbook.createSheet("excel.Response");
        XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        XSSFFont font = (XSSFFont) workbook.createFont();
        font.setBold(true);
        cellStyle.setFont(font);
        XSSFRow row = (XSSFRow) sh.createRow(0);
        XSSFCell cell1 = row.createCell(0);
        cell1.setCellStyle(cellStyle);
        cell1.setCellValue("id");
        XSSFCell cell2 = row.createCell(1);
        cell2.setCellValue("name");
        sh.autoSizeColumn(1);
        XSSFCell cell3 = row.createCell(2);
        cell3.setCellValue("rate");
        XSSFCell cell4 = row.createCell(3);
        sh.autoSizeColumn(1);
        cell4.setCellValue("date");
        sh.autoSizeColumn(1);

        /** Ushbu method markaziy bank url dan json formatidagi Valyuta kursini o'zim yaratgan model class ga o'tkazib beradi **/
        List<ResponseCurrency> responseCurrencies = urlService.generateListFromUrlJson();

        /** List ni model class dagi id fieldi orqali o'sish tartibida saraab beradi **/
            Collections.sort(responseCurrencies,(o1, o2) -> {
                if (o1.getId() < o2.getId()){
                    return -1;
                }else {
                    return 1;
                }
            });

        CellStyle dataStyle = workbook.createCellStyle();
        int rownum = 1;
            for (ResponseCurrency i : responseCurrencies) {
                Row row1 = sh.createRow(rownum++);
                row1.createCell(0).setCellValue(i.getId());
                row1.createCell(1).setCellValue(i.getCcy());
                row1.createCell(2).setCellValue(i.getRate());
                row1.createCell(3).setCellValue(i.getDate());
                Cell dataCell = row.createCell(4);
                dataCell.setCellStyle(dataStyle);
            }

        FileOutputStream fileOutputStream = new FileOutputStream(AppConstants.FILE_PATH + "MyExcel.xlsx");
        workbook.write(fileOutputStream);
        fileOutputStream.close();
        workbook.close();

        return new File(AppConstants.FILE_PATH + "MyExcel.xlsx");
    }

        public File GeneratePdf() throws IOException, DocumentException {

        String file = AppConstants.FILE_PATH + "MyPdf.pdf";

        Document document = new com.itextpdf.text.Document();
        PdfWriter.getInstance(document, Files.newOutputStream(Paths.get(file)));
        document.open();
        PdfPTable table = new PdfPTable(4);

        PdfPCell cell1_1 = new PdfPCell(new Phrase("id"));
        PdfPCell cell1_2 = new PdfPCell(new Phrase("name"));
        PdfPCell cell1_3 = new PdfPCell(new Phrase("rate"));
        PdfPCell cell1_4 = new PdfPCell(new Phrase("date"));

        table.addCell(cell1_1);
        table.addCell(cell1_2);
        table.addCell(cell1_3);
        table.addCell(cell1_4);

        List<ResponseCurrency> responseCurrencies = urlService.generateListFromUrlJson();
            Collections.sort(responseCurrencies,(o1, o2) -> {
                if (o1.getId() < o2.getId()){
                    return -1;
                }else {
                    return 1;
                }
            });

            for (ResponseCurrency i : responseCurrencies) {
                PdfPCell cellNew1 = new PdfPCell(new Phrase(String.valueOf(i.getId())));
                PdfPCell cellNew2 = new PdfPCell(new Phrase(String.valueOf(i.getCcy())));
                PdfPCell cellNew3 = new PdfPCell(new Phrase(String.valueOf(i.getRate())));
                PdfPCell cellNew4 = new PdfPCell(new Phrase(String.valueOf(i.getDate())));

                table.addCell(cellNew1);
                table.addCell(cellNew2);
                table.addCell(cellNew3);
                table.addCell(cellNew4);
            }

        document.add(table);

        document.close();
        return new File(file);
    }
}
