package service;

import model.ResponseCurrency;

import java.io.IOException;
import java.util.List;

public interface UrlService {

    List<ResponseCurrency> generateListFromUrlJson() throws IOException;
}
