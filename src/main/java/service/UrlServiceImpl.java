package service;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.ResponseCurrency;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

public class UrlServiceImpl implements UrlService{
    @Override
    public List<ResponseCurrency> generateListFromUrlJson() throws IOException {

        URL url = new URL("https://cbu.uz/oz/arkhiv-kursov-valyut/json/");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.connect();

        int responseCode = urlConnection.getResponseCode();

        if (responseCode != 200){
            throw  new RuntimeException("ResponseCode:" + responseCode);
        }else {
            StringBuilder stringBuilder = new StringBuilder();
            Scanner scanner = new Scanner(url.openStream());
            while (scanner.hasNextLine()){
                stringBuilder.append(scanner.nextLine());
            }

            scanner.close();

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            return gson.fromJson(String.valueOf(stringBuilder),new TypeToken<List<ResponseCurrency>>(){}.getType());
        }

    }
}
